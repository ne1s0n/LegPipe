#!/bin/bash

# Very basic script that invokes UNEAK pipeline. It requires two command
# line arguments:
# - $1 first argument must be the dataset root. Several folders will be
#      created. Also, compressed fastq files are expected to already be 
#      in $1/Illumina and keyfile with the barcode is expected to be the
#      only file existing in $1/key
# - $2 second argument is the Tassel folder. To run UNEAK we first need
#      to cd there (sigh!). 
#
# Also the world is a place full of despair and sorrow, so no check on
# the argument existances. If they are not there the script will crash.
WORKING_DIR=$1
TASSEL_DIR=$2

#let's ensure we have a place for the logs
LOGS_DIR=$WORKING_DIR/uneak_logs
mkdir -p $LOGS_DIR

#then we start SNP calling. Moving to Tassel directory so 
#we can call the pipeline as a local script
cd $TASSEL_DIR

#the various pipeline steps
STARTING_TIME=$(date)
echo UCreatWorkingDirPlugin
./run_pipeline.pl -fork1 -UCreatWorkingDirPlugin -w $WORKING_DIR -endPlugin -runfork1 >> $LOGS_DIR/UCreatWorkingDirPlugin.log
echo UFastqToTagCountPlugin
./run_pipeline.pl -Xmx15g -Xms2g -fork1 -UFastqToTagCountPlugin -w $WORKING_DIR -e ApeKI -s 2000000000 -endPlugin -runfork1 >> $LOGS_DIR/UFastqToTagCountPlugin.log
echo UMergeTaxaTagCountPlugin
./run_pipeline.pl -Xmx7g -Xms2g -fork1 -UMergeTaxaTagCountPlugin -w $WORKING_DIR -c 5 -m 200000000 -x 100000000 -endPlugin -runfork1 >> $LOGS_DIR/UMergeTaxaTagCountPlugin.log
echo UTagCountToTagPairPlugin
./run_pipeline.pl -Xmx7g -Xms2g -fork1 -UTagCountToTagPairPlugin -e 0.03 -w $WORKING_DIR -endPlugin -runfork1 >> $LOGS_DIR/UTagCountToTagPairPlugin.log
echo UTagPairToTBTPlugin
./run_pipeline.pl -Xmx7g -Xms2g -fork1 -UTagPairToTBTPlugin -w $WORKING_DIR -endPlugin -runfork1 >> $LOGS_DIR/UTagPairToTBTPlugin.log
echo UTBTToMapInfoPlugin
./run_pipeline.pl -Xmx7g -Xms2g -fork1 -UTBTToMapInfoPlugin -w $WORKING_DIR -endPlugin -runfork1 >> $LOGS_DIR/UTBTToMapInfoPlugin.log
echo UMapInfoToHapMapPlugin
#./run_pipeline.pl -Xmx7g -Xms2g -fork1 -UMapInfoToHapMapPlugin -w $WORKING_DIR -mnMAF 0.05 -mxMAF 0.5 -mnC 0 -mxC 1 -endPlugin -runfork1 >> $LOGS_DIR/UMapInfoToHapMapPlugin.log
#here Xuehui says to use mnMAF 0.01 -----------------------------------------------------V
./run_pipeline.pl -Xmx7g -Xms2g -fork1 -UMapInfoToHapMapPlugin -w $WORKING_DIR -mnMAF 0.01 -mxMAF 0.5 -mnC 0 -mxC 1 -endPlugin -runfork1 >> $LOGS_DIR/UMapInfoToHapMapPlugin.log
ENDING_TIME=$(date)

echo STARTING_TIME: $STARTING_TIME
echo ENDING_TIME: $ENDING_TIME

 
