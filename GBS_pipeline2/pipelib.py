import csv
import datetime
import difflib
import glob
import inspect
import math
import os
import random
import string
import subprocess
import sys

class GBSpipeline:
  """ a GBS pipeline """
  
  #the conf object, containing everything coming from conf file
  conf = None
  
  #the unique id for this run
  run_id = None

  #the compressed fastq files (with full path)
  fastq_gz_files = None
  
  #the uncompressed fastq files (with full path)
  fastq_files = None
  
  #the compressed and demultiplexed fastq files (with full path)
  fastq_dem_gz_files = None
  
  #the compressed fastq files for uneak pipeline (with full path)
  uneak_fastq_files = None

  def __ensure_path(self, path):
    """ensures that the passed path does exist"""
    if not os.path.exists(path):
        os.makedirs(path)

  def __list_files_suffix(self, path, suffix):
    """returns a list of all files (with full path) in the passed path 
    and having the passed suffix. If the path does not exists returns
    an empty list"""
    
    #are we working with an existing path?
    if not os.path.isdir(path):
      return list()
    
    #the path exists and is a folder, let's collect the files from it
    file_list = list()
    for file_current in os.listdir(path):
      if file_current.endswith(suffix):
        file_list.append(path + file_current)
    #done
    return file_list
    
  def __refresh_files_list(self):
    """updates all the file lists"""
    
    #compressed fastq
    self.fastq_gz_files = self.__list_files_suffix(path = self.conf.get('DEFAULT', 'fastq_folder'), suffix = '.fastq.gz')
    
    #uncompressed fastq
    self.fastq_files = self.__list_files_suffix(self.conf.get('UNCOMPRESS_FASTQ', 'out_folder'), '.fastq')
    
    #compressed demultiplexed files
    self.fastq_dem_gz_files = self.__list_files_suffix(self.conf.get('DEMULTIPLEX', 'out_folder'), '.fastq.gz')
    
    #compressed fastq (with .txt extension) for UNEAK pipeline
    self.uneak_fastq_files = self.__list_files_suffix(self.conf.get('DEFAULT', 'root') + 'Illumina/', '_fastq.txt.gz')

  def __id_generator(self):
    """generates a string with current date/time plus a random
    alphanumeric queue, unless an old id is specified in conf"""
    
    if self.conf.get('DEFAULT', 'run_id') == 'FALSE':
      timeblob = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d_%H-%M-%S') 
      size = 6
      chars = string.ascii_uppercase + string.digits
      return timeblob + '_' + ''.join(random.choice(chars) for _ in range(size))
    else:
      return self.conf.get('DEFAULT', 'run_id')

  def __init__(self, conf):
    """folder creation and general data preparation"""
    
    #saving a copy of conf in the pipeline object
    self.conf = conf
    
    #generating the unique id, with time and a random queue
    self.run_id = self.__id_generator()
    
    #a bit of interface
    print '[init] GBS pipeline initialization for run ' + self.run_id
    
    #tmp folder (workbench)
    self.__ensure_path(self.conf.get('DEFAULT', 'tmp'))

    #logs folder
    self.__ensure_path(self.conf.get('DEFAULT', 'log_folder'))

    #stats folder
    self.__ensure_path(self.conf.get('DEFAULT', 'stats_folder'))

    #reads stats subfolder
    self.__ensure_path(self.conf.get('DEFAULT', 'stats_folder') + 'reads_stats/')

    #reference genome folder
    self.__ensure_path(self.conf.get('DEFAULT', 'root') + 'refGenome/')
    
    #aligner output folder
    self.__ensure_path(self.conf.get('ALIGN', 'out_folder'))
    
    #variant caller output folder
    self.__ensure_path(self.conf.get('CALL_VARIANTS', 'out_folder'))
    
    #genotypes folder
    self.__ensure_path(self.conf.get('FILTER_VARIANTS', 'out_folder'))

    #demultiplexed folder
    self.__ensure_path(self.conf.get('DEMULTIPLEX', 'out_folder'))
    
    #joined fastq folder
    self.__ensure_path(self.conf.get('JOIN_MULTI_FASTQ', 'multi_fastq_folder'))
    
    #uncompressed fastq folder
    self.__ensure_path(self.conf.get('UNCOMPRESS_FASTQ', 'out_folder'))
    
    #finally, we create the lists of interesting files
    self.__refresh_files_list()

  def __run_and_log(self, cmd_list, report_cmd = True, report_timeblob = False):
    #getting the name of the caller
    caller = inspect.stack()[1][3]
    
    #building two files, one for stdout, one for stderr
    log_folder = self.conf.get('DEFAULT', 'log_folder')
    
    standard_output = file(log_folder + self.run_id + '_' + caller + '.out', 'a')
    standard_error = file(log_folder + self.run_id + '_' + caller + '.err', 'a')
    
    #log header/separator
    if report_timeblob :
      blob = '===================== ' + self.run_id + '\n'
      standard_output.write(blob)
      standard_output.flush()
    
    #executed command
    cmd = ' '.join(cmd_list)
    if report_cmd :
      standard_output.write('[cmd] ' + cmd +'\n')
      standard_output.flush()

    #executing command
    subprocess.call(cmd, shell = True, stdout = standard_output, stderr = standard_error)
    
    #closing the log files
    standard_output.close()
    standard_error.close()
    
  def __get_called_outfile(self):
    return self.conf.get('CALL_VARIANTS', 'out_folder') + \
        'freeBayesOutput' + \
        '_run-' + self.run_id + \
        '.vcf.gz'
  
  def uneak(self):
    """calls the UNEAK pipeline"""
    
    #should we do something?
    if (self.conf.get('UNEAK', 'active') != 'TRUE'):
      print '[uneak] skipped'
      return
    
    #UNEAK requires compressed .fastq.gz in the <root>/Illumina folder, but
    #for some reason the extension should be ".txt.gz"
    #We just list that we have some to work on
    self.__refresh_files_list()
    
    #do we have anything to process?
    l = len(self.uneak_fastq_files)
    if l == 0:
      print '[uneak] nothing to process'
      return
    print '[uneak] Calling UNEAK on ' + str(l) + ' files: ' + ", ".join(self.uneak_fastq_files)
      
    #example command: ./uneak.sh <root_folder> <tassel_folder>
    cmd = list()

    #this is a local script doing all the necessary steps
    cmd.append(self.conf.get('TOOLS', 'legpipe_folder') + '/uneak.sh')
    cmd.append(self.conf.get('DEFAULT', 'root'))
    cmd.append(self.conf.get('TOOLS', 'tassel_folder'))
    self.__run_and_log(cmd)

  def uncompress_fastq(self):
    #should we do something?
    if (self.conf.get('UNCOMPRESS_FASTQ', 'active') != 'TRUE'):
      print '[uncompress_fastq] skipped'
    else:
      #outfolder
      outfolder = self.conf.get('UNCOMPRESS_FASTQ', 'out_folder')
      
      #because of gzip interface, we need to proceed one file at a time
      #to be able to keep compressed copies and have uncompressed
      #files in another folder
      for f in self.fastq_gz_files:
        #separating filename from folder name
        fname = os.path.basename(f)
        
        #a bit of interface
        print '[uncompress_fastq] uncompressing ' + fname
        
        #removing the final '.gz'
        fname = fname[:-3]
        
        #Example command: zcat infile > outfile
        cmd = list()
        cmd.append('zcat ' + f + ' >')
        cmd.append(outfolder + fname)
        self.__run_and_log(cmd)
        
      #once done compressing, let's just update the lists
      self.__refresh_files_list()

  def keyfile_to_barcodes(self):
    """creates a barcode file from UNEAK key file"""
    
    if (self.conf.get('KEYFILE_TO_BARCODES', 'active') != 'TRUE'):
      print '[KEYFILE_TO_BARCODES] skipped'
      return

    #uneak keyfile should be the only file present in the /key folder, by default
    infile_name = self.__list_files_suffix(self.conf.get('DEFAULT', 'root') + 'key/', '')
    if len(infile_name) == 0:
      print '[KEYFILE_TO_BARCODES] *ERROR* no files in key subfolder'
      return
      
    if len(infile_name) > 1:
      print '[KEYFILE_TO_BARCODES] *WARNING* more than one file in key subfolder. Using ', infile_name[0]

    infile_name = infile_name[0]
    print '[KEYFILE_TO_BARCODES] converting to usable barcode format UNEAK keyfile ' + infile_name
    
    #we need to create (possibly overwrite) the barcode file for demultiplexing
    outfile_name = self.conf.get('DEMULTIPLEX', 'barcode_file')
    
    with open(infile_name, 'r') as csvinput:
      with open(outfile_name, 'w') as csvoutput:
        #the skipinitialspace is useful for space-aligned files (sometimes
        #found around)
        reader = csv.DictReader(csvinput,  delimiter=' ', skipinitialspace=True)
        
        #creating the output writer
        writer = csv.DictWriter(csvoutput, fieldnames=['Samplename', 'Barcode'], delimiter=' ', quoting=csv.QUOTE_NONE)
        
        #each line goes to the barcode file
        for row in reader:
          #no header
          writer.writerow({'Samplename': row['Samplename'], 'Barcode': row['Barcode']})
  
  def demultiplex(self):
    """demultiplexes fastq files and compress results"""

    #should we do something?
    if (self.conf.get('DEMULTIPLEX', 'active') != 'TRUE'):
      print '[demultiplex] skipped'
      return
        
    #just to be sure, let's update the lists
    self.__refresh_files_list()
    
    #do we have anything to process?
    l = len(self.fastq_gz_files)
    if l == 0:
      print '[demultiplex] nothing to process'
      return
    print '[demultiplex] demultiplexing ' + str(l) + ' files'

    
    #for each available uncompressed file
    for f in self.fastq_gz_files:
      print '[demultiplex] demultiplexing ' + f
    
      #separating filename and removing extension (.fastq.gz)
      fname = os.path.basename(f)
      fname = fname[:-9]
    
      #fastq-multx syntax
      #-B <barcode.file> with: id sequence
      #-m N  Allow up to N mismatches, as long as they are unique (1)
      #-d N  Require a minimum distance of N between the best and next best (2)
      #-q N  Require a minimum phred quality of N to accept a barcode base (0)
      #Usage: fastq-multx -B <barcodes.file> <read1.fq> -o r1.%.fq.gz
      cmd = list()
      cmd.append(self.conf.get('TOOLS', 'fastq-multx'))
      cmd.append('-B ' + self.conf.get('DEMULTIPLEX', 'barcode_file'))
      cmd.append('-m ' + self.conf.get('DEMULTIPLEX', 'fastq-multx_m'))
      cmd.append('-d ' + self.conf.get('DEMULTIPLEX', 'fastq-multx_d'))
      cmd.append(f)
      cmd.append('-o ' + self.conf.get('DEMULTIPLEX', 'out_folder') + fname + '.%.fastq.gz')
      self.__run_and_log(cmd)
      
  def demultiplex_stats(self):
    #should we do something?
    if (self.conf.get('DEMULTIPLEX_STATS', 'active') != 'TRUE'):
      print '[demultiplex_stats] skipped'
      return

    #let's ensure lists are fresh
    self.__refresh_files_list()
    
    #do we have anything to process?
    l = len(self.fastq_dem_gz_files)
    if l == 0:
      print '[demultiplex_stats] nothing to process'
      return
    print '[demultiplex_stats] extracting statistics for ' + str(l) + ' files'
    
    #total stats, stored into a dict
    total_stats = {}
    
    #for each demultiplexed file
    for f in self.fastq_dem_gz_files:
      #f is in the form <path>/<source file>.<sample>.fastq.gz
      fname = os.path.basename(f)
      [source_file, sample] = os.path.splitext(fname[:-9])
      sample = sample[1:]
      print '[demultiplex_stats] examining sample ' + sample + ' from ' + source_file
      
      #output is redirected to a tmp file for postprocessing
      tmpfile = self.conf.get('DEFAULT', 'tmp') + self.run_id + '_stats.txt'

      #example command: fastq-stats -s 0 <infile.fastq.gz>
      #the "-s 0" option remove duplicates breakdown and keeps the output
      #in a nice <stat>tab<number> format
      cmd = list()
      cmd.append(self.conf.get('TOOLS', 'fastq-stats'))
      cmd.append('-s 0')
      cmd.append(f)
      cmd.append('> ' + tmpfile)
      self.__run_and_log(cmd)
      
      #reading the stat file to a dictionary
      sample_stats = {}
      sample_stats['file'] = fname
      sample_stats['code'] = sample
      sample_stats['source'] = source_file
      with open(tmpfile) as infile:
        for line in infile:
          (stat, val) = line.split('\t')
          sample_stats[stat] = val.strip()
      
      #and adding to the others
      total_stats[f] = sample_stats
    
    #outputing stats
    outfile_name = self.conf.get('DEFAULT', 'stats_folder') + 'demultiplex_stats.csv'
    with open (outfile_name, 'w') as outfile:
      writer = csv.DictWriter(outfile, 
        fieldnames = sample_stats.keys(), #fieldnames are taken from one of the stats
        delimiter=',', quotechar='"')
      writer.writeheader()
      for s in total_stats:
        writer.writerow(total_stats[s])
    
    #calling R script to create plots and summary tables
    print '[demultiplex_stats] generating plots and summary tables'
    cmd = list()
    cmd.append(self.conf.get('TOOLS', 'Rscript'))
    cmd.append(self.conf.get('TOOLS', 'legpipe_folder') + 'readsReport.R')
    cmd.append(outfile_name) #stats file, just created
    cmd.append(self.conf.get('DEFAULT', 'datadescription_file')) #data description, with plates number and grouping
    cmd.append(self.conf.get('DEFAULT', 'stats_folder') + 'reads_stats/') #where to save
    self.__run_and_log(cmd)
    
  def join_multi_fastq(self):
    """Post demultiplexing, joins several fastq referring to the same sample, based on filenames"""
    
    #should we do something?
    if (self.conf.get('JOIN_MULTI_FASTQ', 'active') != 'TRUE'):
      print '[join_multi_fastq] skipped'
      return

    #just to be sure, let's update the lists
    self.__refresh_files_list()

    #reading sample names from barcode file
    barcode_filename = self.conf.get('DEMULTIPLEX', 'barcode_file')

    with open(barcode_filename, 'r') as barcode_ptr:
      barcode_reader = csv.DictReader(barcode_ptr,  delimiter=' ', skipinitialspace=True, fieldnames=['sample', 'barcode'])
        
      print '[join_multi_fastq] joining the following files and moving the old one in /fastq_separated:'
      #each line has (samplename, barcode)
      for row in barcode_reader:
        #retrieving the list of files to be joined
        files = self.__list_files_suffix(
          path = self.conf.get('DEMULTIPLEX', 'out_folder'),
          suffix = row['sample'] + '.fastq.gz')

        print ' - sample ' + row['sample'] + ':' + ', '.join(files)
        
        if len(files) == 0:
          #no fastq associated to the current sample
          continue
        
        #the concatenation command
        cmd = list()
        cmd.append('cat')
        cmd.extend(files)
        cmd.append('> ' + self.conf.get('DEMULTIPLEX', 'out_folder') + row['sample'] + '.joined.fastq.gz')
        self.__run_and_log(cmd)
        
        #moving the original (now concatenated) fastqs, to the joined folder
        cmd = list()
        cmd.append('mv')
        cmd.extend(files)
        cmd.append(self.conf.get('JOIN_MULTI_FASTQ', 'multi_fastq_folder'))
        self.__run_and_log(cmd)

  def create_ref_genome(self):
    """creates a reference genome and a descriptive file"""
    
    #should we do something?
    if (self.conf.get('REFERENCE', 'active') != 'TRUE'):
      print '[createRefGenome] skipped'
      return

    #reference genome folder
    refpath = self.conf.get('DEFAULT', 'root') + 'refGenome/'

    #some interface to user
    print '[createRefGenome] parsing HapMap.fas.txt into a reference genome'
    print '[createRefGenome] results in ' + refpath
    
    #building names for output files
    fin_name = self.conf.get('DEFAULT', 'root') + '/hapMap/HapMap.fas.txt'
    ref_name = refpath + 'reference.fasta'
    desc_name = refpath + 'markers_description.csv'

    #opening files 
    fin = open(fin_name, 'r')
    fout_ref = open(ref_name, 'w')
    fout_desc = open(desc_name, 'w')

    #putting header were needed
    fout_desc.write('marker,neighbour1,polymorphism,neighbour2,complete_sequence\n')

    #this particular fastq must be read in blocks of four lines, two for
    #queries and two for hits
    lab_query = fin.readline()
    seq_query = fin.readline()
    lab_hit   = fin.readline()
    seq_hit   = fin.readline()

    while lab_query != '':
      #extracting true label and markers length
      #pieces[0] : >marker_label
      #pieces[1] : the string 'query'
      #pieces[2] : actual length of the sequence (without polyA padding)
      pieces = lab_query.split('_')
      marker = pieces[0]
      size = int(pieces[2])
      
      #isolating the two alternative sequences, without polyA
      seq_query = seq_query[:size]
      seq_hit   = seq_hit[:size]
      
      #they differ only for the SNP, let's find where it is
      d = difflib.ndiff(seq_query, seq_hit)
      for i, s in enumerate(d) :
        if s[0] != ' ':
          break
      
      #now we extract the neighborhoods
      neigh1 = seq_query[:i]
      neigh2 = seq_query[i+1:]
      
      #and we are ready for output
      
      #reference genome: a fasta file with only one variant (the query one)
      fout_ref.write(marker + '\n')
      fout_ref.write(neigh1 + seq_query[i] + neigh2 + '\n')
      
      #in the description file, we put the sequence with the highlighed polymorphism
      s = marker[1:] + ',' 
      s = s + neigh1 + ',' + seq_query[i] + '/' + seq_hit[i] + ',' + neigh2 + ','
      s = s + neigh1 + '[' + seq_query[i] + '/' + seq_hit[i] + ']' + neigh2
      fout_desc.write(s + '\n')

      #lets not forget to read the new block of four lines
      lab_query = fin.readline()
      seq_query = fin.readline()
      lab_hit   = fin.readline()
      seq_hit   = fin.readline()

    #closing and saving 
    fin.close()
    fout_ref.close()
    fout_desc.close()

  def index_genome(self):
    """indexes the genome fasta both with samtools and with bwa"""
    
    #ref genome path
    ref_file = self.conf.get('DEFAULT', 'root') + 'refGenome/reference.fasta'
    
    #indexing with samtools
    if (self.conf.get('INDEX', 'samtools_active') != 'TRUE'):
      print '[indexGenome] samtools skipped'
    else:
      print '[indexGenome] samtools indexing'
      cmd = list()
      cmd.append(self.conf.get('TOOLS', 'samtools') + ' faidx')
      cmd.append(ref_file)
      self.__run_and_log(cmd)
      
    #indexing with BWA
    if (self.conf.get('INDEX', 'bwa_active') != 'TRUE'):
      print '[indexGenome] bwa skipped'
    else:
      print '[indexGenome] bwa indexing'
      cmd = list()
      cmd.append(self.conf.get('TOOLS', 'bwa') + ' index')
      cmd.append(ref_file)
      self.__run_and_log(cmd)

  def align(self):
    """for each sample all demultiplexed files are aligned
    on the reference genome with bwa"""
    
    #should we do something?
    if (self.conf.get('ALIGN', 'active') != 'TRUE'):
      print '[align] skipped'
      return
    
    #reference genome file, file is created by the pipeline
    ref_file = self.conf.get('DEFAULT', 'root') + 'refGenome/reference.fasta'
    
    #retrieving list of files to be aligned (one file per sample)
    samples = self.__list_files_suffix(self.conf.get('DEMULTIPLEX', 'out_folder'), '.fastq.gz')
        
    #for each combination of original fastq and sample we check for possible
    #demultiplexed files, and we join everything together
    for sample in samples:
      #from filename to sample name (for log readibility)
      samplename = os.path.basename(sample)
      samplename = samplename[:-9]  #removing .fastq.gz

      #a bit of user interface
      print '[align] aligning', samplename

      #BWA output: .sam, .bam, sorted .bam, index
      bwa_sam          = self.conf.get('DEFAULT', 'tmp') + 'bwa_' + samplename + '.sam'
      bwa_bam          = self.conf.get('DEFAULT', 'tmp') + 'bwa_' + samplename + '.bam'
      bwa_bam_sortPref = self.conf.get('DEFAULT', 'tmp') + 'bwa_' + samplename + '.sorted' #this is a prefix, no extension
      bwa_bam_sort_RG             = self.conf.get('ALIGN', 'out_folder') + 'bwa_' + samplename + '.sorted.RG.bam'
      bwa_bam_sort_RG_nodupes     = self.conf.get('ALIGN', 'out_folder') + 'bwa_' + samplename + '.sorted.RG.nodupes.bam'
      bwa_bam_sort_RG_nodupes_idx = bwa_bam_sort_RG_nodupes + '.bai'
      
      #BWA align. The "dummylib" tag is necessary for a known bug in 
      #samtools generating defaults with rmdup if no LB is present
      #Example command: bwa mem -t 8 -R @RG\tID:foo\tLB:dummylib\tSM:bar ref.fasta sample200.fastq > bwa_output.sam
      cmd = list()
      cmd.append(self.conf.get('TOOLS', 'bwa') + ' mem')
      cmd.append('-t ' + self.conf.get('DEFAULT', 'threads'))
      cmd.append(ref_file)
      cmd.append(sample)
      cmd.append('> ' + bwa_sam)
      self.__run_and_log(cmd)

      #convert .sam -> .bam
      #Example command: samtools view -bS bwa_output.sam > bwa_output.bam
      cmd = list()
      cmd.append(self.conf.get('TOOLS', 'samtools') + ' view -bS ' + bwa_sam)
      cmd.append('> ' + bwa_bam)
      self.__run_and_log(cmd)
    
      #sorting .bam
      #Example command: samtools sort --threads 8 -o outfile_sorted bwa_output.bam
      cmd = list()
      cmd.append(self.conf.get('TOOLS', 'samtools') + ' sort')
      cmd.append('--threads ' + self.conf.get('DEFAULT', 'threads'))
      cmd.append('-o ' + bwa_bam_sortPref + '.bam')
      cmd.append(bwa_bam)
      self.__run_and_log(cmd)
      
      #using picard AddOrReplaceReadGroups, adding @RG to bam file
      #example command java -jar picard.jar AddOrReplaceReadGroups I=newbam.bam O=waa.bam RGID=4 RGLB=foo RGPL=bar RGPU=fdafdas RGSM=ooooooooo
      cmd = list()
      cmd.append(self.conf.get('TOOLS', 'picard') + ' AddOrReplaceReadGroups')
      cmd.append('I=' + bwa_bam_sortPref + '.bam')
      cmd.append('O=' + bwa_bam_sort_RG)
      cmd.append('RGID=' + samplename)
      cmd.append('RGLB=' + 'dummylib')
      cmd.append('RGSM=' + 'sample_' + samplename)
      cmd.append('RGPL=' + 'ILLUMINA') #no need to read a parameter here, 
                                       #since the whole pipeline is quite
                                       #Illumina oriented
      cmd.append('RGPU=' + 'dummyPU')  #currently there is no way to put
                                       #a meaningful value here
      self.__run_and_log(cmd)
      
      #using picard MarkDuplicates, removing duplicates reads
      #example command java -jar picard.jar MarkDuplicates I=bwa_PeaS78_lane_4_1.PeaS78.fastq.gz.sorted.bam O=nodupes.bam M=res.txt
      cmd = list()
      cmd.append(self.conf.get('TOOLS', 'picard') + ' MarkDuplicates')
      cmd.append('I=' + bwa_bam_sort_RG)
      cmd.append('O=' + bwa_bam_sort_RG_nodupes)
      cmd.append('M=' + '/dev/null') #metrics file is ignored
      #cmd.append('REMOVE_DUPLICATES=true') #should we remove the duplicates?
      self.__run_and_log(cmd)

      #creating index for sorted, dupeless bam
      #Example command: samtools index bwa_output_sorted.bam > bwa_output_sorted.bam.bai
      cmd = list()
      cmd.append(self.conf.get('TOOLS', 'samtools') + ' index ' + bwa_bam_sort_RG_nodupes)
      cmd.append(' > ' + bwa_bam_sort_RG_nodupes_idx)
      self.__run_and_log(cmd)
      
      #removing unnecessary files
      #self.__run_and_log(['rm ' + fastq_uncompressed])
      self.__run_and_log(['rm ' + bwa_sam])
      self.__run_and_log(['rm ' + bwa_bam])
      self.__run_and_log(['rm ' + bwa_bam_sort_RG])
      self.__run_and_log(['rm ' + bwa_bam_sortPref + '.bam'])
        
  def call_variants(self):
    """call freeBayes on all the sorted (and indexed) bam present in
    the aligned folder"""
    
    #should we do something?
    if (self.conf.get('CALL_VARIANTS', 'active') != 'TRUE'):
      print '[call_variants] skipped'
      return
    print '[call_variants] calling variants with freeBayes'
    
    #reference genome file
    ref_file = self.conf.get('DEFAULT', 'root') + 'refGenome/reference.fasta'

    #this file will contain the list of bam file, post alignment
    bam_list_file = self.conf.get('CALL_VARIANTS', 'out_folder') + 'sorted_bam_list.txt'
    
    #retrieving the list of available .bam files
    bam_list = glob.glob(self.conf.get('ALIGN', 'out_folder') + '*.bam')
    
    #sorting alphabetically, so same samples (from multiple lanes)
    #end up close to each other
    bam_list.sort()
    
    #call batch size
    batch_size = int(self.conf.get('CALL_VARIANTS', 'sample_per_batch'))
    if batch_size == 0 :
      batch_size = len(bam_list)
    
    #how many batches do we have
    batch_num = int(math.ceil(float(len(bam_list)) / batch_size))
    
    #we proceed from batch to batch
    for b in range(0, batch_num):
      #defining the batch
      from_idx = b * batch_size
      to_idx   = b * batch_size + batch_size
      batch = bam_list[from_idx:to_idx]
      
      #creating the file listing the .bam files from the current batch
      with open(bam_list_file, 'w') as outfile :
        for bam_file in batch:
          outfile.write(bam_file + '\n')
      
      #building output .vcf.gz name, using the run id plus the batch number
      vcf_filename = self.conf.get('CALL_VARIANTS', 'out_folder') + \
        'freeBayesOutput' + \
        '_run-' + self.run_id + \
        '_batch-' + str(b+1) + \
        '.vcf.gz'
      
      #interface for the user
      print '[call_variants] batch ' + str(b+1) + '/' + str(batch_num)
      
      #at this point we have aligned all files and we have a collection of
      #sorted and indexed bam. Let's call the variants with freeBayes
      #Example command: freebayes -f ref.fasta --bam-list FILE | bgzip > freeBayes_output.vcf.gz
      cmd = list()
      cmd.append(self.conf.get('TOOLS', 'freebayes'))
      cmd.append('-f ' + ref_file)
      cmd.append(self.conf.get('CALL_VARIANTS', 'extraparms')) #user provided parameters
      cmd.append('--bam-list ' + bam_list_file)
      cmd.append(' | bgzip > ' + vcf_filename)
      self.__run_and_log(cmd)
      
      #indexing the resulting vcf files
      #Example command: tabix -p vcf file.vcf.gz
      cmd = list()
      cmd.append(self.conf.get('TOOLS', 'tabix'))
      cmd.append('-p vcf ' + vcf_filename)
      self.__run_and_log(cmd)

    #the final vcf filename 
    outfile = self.__get_called_outfile()
    
    #we need to merge all vcf files in a single output one, but only if
    #we had actually more than one batch. Otherwise is a matter of simple
    #file renaming (mv <oldfilename> <newfilename>
    if batch_num == 1:
      cmd = list()
      cmd.append('mv')
      cmd.append(self.conf.get('CALL_VARIANTS', 'out_folder') + \
          'freeBayesOutput' + \
          '_run-' + self.run_id + \
          '_batch-1.vcf.gz')
      cmd.append(outfile)
      self.__run_and_log(cmd)
    else:
      #example command vcf-merge [OPTIONS] file1.vcf file2.vcf.gz ... > out.vcf
      
      #the merge command
      #note: we now use vcf-merge, but it is very slow. There is a tool in
      #htslib which is supposed to be way faster. To be investigated.
      #Example command: vcf-merge input_files*.vcf.gz | gzip > output_file.vcf.gz
      cmd = list()
      cmd.append(self.conf.get('TOOLS', 'vcf-merge'))
      cmd.append(self.conf.get('CALL_VARIANTS', 'out_folder') + \
          'freeBayesOutput' + \
          '_run-' + self.run_id + \
          '_batch-*.vcf.gz')
      cmd.append(' | bgzip > ' + outfile)
      self.__run_and_log(cmd)
    
      #indexing final vcf (joined batches) with tabix
      #Example command: tabix -p vcf file.vcf.gz
      cmd = list()
      cmd.append(self.conf.get('TOOLS', 'tabix'))
      cmd.append('-p vcf ' + outfile)
      self.__run_and_log(cmd)
      
  def cleanup(self):
    """remove everything from selected folders"""

    #tmp (workbench) folder
    if (self.conf.get('CLEANUP', 'tmp_active') == 'TRUE'):
      print '[cleanup] tmp folder'
      cmd = list()
      cmd.append('rm ' + self.conf.get('DEFAULT', 'tmp') + '*')
      self.__run_and_log(cmd)  

    #tmp (workbench) folder
    if (self.conf.get('CLEANUP', 'align_active') == 'TRUE'):
      print '[cleanup] aligned .bam folder'
      cmd = list()
      cmd.append('rm ' + self.conf.get('ALIGN', 'out_folder') + '*')
      self.__run_and_log(cmd)  
      
  def filter_variants(self):
    """from vcf to other formats, with optional filtering"""
    
    #should we do something?
    if (self.conf.get('FILTER_VARIANTS', 'active') != 'TRUE'):
      print '[filter_variants] skipped'
      return
    print '[filter_variants] filtering vcf into handy formats'
    
    #freeBayes (compressed) output
    fb_out = self.__list_files_suffix(self.conf.get('CALL_VARIANTS', 'out_folder'), '.vcf.gz')
    
    if len(fb_out) == 0:
      print '[filter_variants] *ERROR* no files in called folder'
      return
    
    if len(fb_out) > 1:
      print '[filter_variants] *ERROR* more than one vcf.gz in called folder'
      return
    
    #if we get here we can proceed
    fb_out = fb_out[0]
    
    #target folder and file prefix
    tf = self.conf.get('FILTER_VARIANTS', 'out_folder') + 'genos'
    
    #extracting a matrix of 0/1/2
    #Example command: vcftools --012 --gzvcf <infile> --out <outfile_prefix>
    cmd = list()
    cmd.append(self.conf.get('TOOLS', 'vcftools'))
    cmd.append('--gzvcf ' + fb_out + ' --out ' + tf)  
    cmd.append(self.conf.get('FILTER_VARIANTS', 'extraparms'))
    self.__run_and_log(cmd)
    
    #output in plink format
    #Example command: vcftools --plink-tped --gzvcf <infile> --out <outfile_prefix> [[extra parameters]]
    #cmd = list()
    #cmd.append(self.conf.get('TOOLS', 'vcftools'))
    #cmd.append('--plink-tped --gzvcf ' + fb_out + ' --out ' + tf)
    #cmd.append(self.conf.get('FILTER_VARIANTS', 'extraparms_012'))
