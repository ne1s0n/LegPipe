import ConfigParser
import sys
from pipelib import GBSpipeline 

#conf filename is the one and only accepted command line parameter
conf_filename = sys.argv[1]

conf = ConfigParser.ConfigParser()
conf.read(conf_filename)

#the actual pipeline, init
pipe = GBSpipeline(conf)

#doing the steps
pipe.uneak()
pipe.create_ref_genome()
pipe.index_genome()
pipe.uncompress_fastq()
pipe.keyfile_to_barcodes()
pipe.demultiplex()
pipe.demultiplex_stats()
pipe.join_multi_fastq()
pipe.align()
pipe.call_variants()
pipe.filter_variants()
pipe.cleanup()
