# library management ------------------------------------------------------
#adding the LEGPIPE_R_LIB path to existing libPaths. In this
#way it is possible to reference local/user installed libraries
#in multiuser environments
.libPaths(c(Sys.getenv('LEGPIPE_R_LIB'), .libPaths()))

#the libraries needed for this script
library(ggplot2)
library(reshape)

# command line parsing ----------------------------------------------------
cl.args = commandArgs(trailingOnly = TRUE)

#stats file, created by pipeline
readsStats.file = cl.args[1]
#data description, with plates number and grouping
sampleInventory.file = cl.args[2]
#where to save results
outfolder = cl.args[3]

# data preparation --------------------------------------------------------
#making sure out folder is there
dir.create(outfolder, showWarnings = FALSE, recursive = TRUE)

#reading input data
readsStats = read.csv(readsStats.file)
sampleInventory = read.csv(sampleInventory.file)

#join the two dataset so each demultiplexed file has its own plate and group
df = merge(x=readsStats, y=sampleInventory, by = 'code', all.x = TRUE)

#have we missed something?
m = is.na(df$sample) & (df$code != 'unmatched')
if (sum(m) > 0){
  msg = paste(df[m, 'code'], collapse = ', ')
  msg = paste('The following codes from demultiplexing stats do not correspond to any genotypes\n', msg)
  warning(msg)
}

#sorting barcode by length to provide better barcode stats
df$barcode = as.character(df$barcode)
barcodes = unique(df$barcode)
barcodes = barcodes[order(nchar(barcodes))]
df$barcode = factor(df$barcode, barcodes)

# total and unmatched reads -----------------------------------------------
#total reads by original source
reads.by.lane = aggregate(x=df$reads, by=list(df$source), FUN = 'sum')
colnames(reads.by.lane) = c('source', 'reads')

#unmatched reads are assessed and the discarded
for (i in 1:nrow(reads.by.lane)){
  sel = (df$source == reads.by.lane[i, 'source']) & (df$code == 'unmatched')
  reads.by.lane[i, 'unmatched'] = df[sel, 'reads']
}
reads.by.lane$unmatched.perc = reads.by.lane$unmatched / reads.by.lane$reads

#saving in table form
tablename = file.path(outfolder, 'tab_reads_by_source.csv')
write.csv(x = reads.by.lane, file = tablename, row.names = FALSE)

#saving in plot form
df.gg = melt(reads.by.lane, id=c('source'))
df.gg = subset(df.gg, variable != 'unmatched.perc')

p = ggplot(df.gg, aes(x=source, y=value, fill=variable)) + 
  geom_bar(position = 'dodge', stat='identity') +
  theme(axis.title.x = element_blank(), axis.title.y = element_blank()) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust=0.5)) +
  ggtitle('Reads by source')
plotname = file.path(outfolder, 'plot_reads_by_source.png')
ggsave(filename = plotname, plot=p)

#removing unmatched reads
df = df[df$code != 'unmatched',]

# output tables with several aggregations ---------------------------------
#we aggregate by each column of sampleInventory except "code"
for (v in colnames(sampleInventory)){
  if (v == 'code') next
  
  #first stat: mean
  res = aggregate(x=df$reads, by=list(df[,v]), FUN=mean)
  colnames(res) = c(v, 'mean')
  
  #adding stats
  res$min = aggregate(x=df$reads, by=list(df[,v]), FUN=min)[,2]
  res$max = aggregate(x=df$reads, by=list(df[,v]), FUN=max)[,2]
  
  #saving
  tablename = paste('', 'tab_reads_by_', v, '.csv')
  tablename = file.path(outfolder, tablename)
  write.csv(x = res, file = tablename, row.names = FALSE)
}

# aggregate by plate ------------------------------------------
p = ggplot(df, aes(x=plate, y=reads, group=plate)) + geom_boxplot() +
  theme(axis.title.x = element_blank()) +
  ggtitle('Reads by plate')
plotname = file.path(outfolder, 'plot_reads_by_plate.png')
ggsave(filename = plotname, plot=p)

# aggregate by group ------------------------------------------
p = ggplot(df, aes(x=group, y=reads, group=group)) + geom_boxplot() +
  theme(axis.title.x = element_blank()) +
  ggtitle('Reads by group')
plotname = file.path(outfolder, 'plot_reads_by_group.png')
ggsave(filename = plotname, plot=p)

# aggregate by sample, group coloring -------------------------------------
p = ggplot(df, aes(x=sample, y=reads, group=sample, fill=group, color=group)) + geom_boxplot() +
  theme(axis.title.x = element_blank()) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust=0.5)) +
  theme(panel.grid.minor.x = element_blank()) +
  theme(panel.grid.major.x = element_blank()) +
  theme(legend.position = 'bottom') + 
  ggtitle('Reads by sample')
plotname = file.path(outfolder, 'plot_reads_by_sample.png')

#rule of thumb: one inch for every 10 elements
num = length(unique(df$sample))
ggsave(filename = plotname, plot=p, width = num / 10)

# aggregate by barcode ----------------------------------------------------
p = ggplot(df, aes(x=barcode, y=reads, group=barcode)) + geom_boxplot() +
  theme(axis.title.x = element_blank()) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust=0.5)) +
  theme(panel.grid.minor.x = element_blank()) +
  theme(panel.grid.major.x = element_blank()) +
  ggtitle('Reads by barcode')
plotname = file.path(outfolder, 'plot_reads_by_barcode.png')

#rule of thumb: one inch for every 10 elements
num = length(unique(df$barcode))
ggsave(filename = plotname, plot=p, width = num / 10)
