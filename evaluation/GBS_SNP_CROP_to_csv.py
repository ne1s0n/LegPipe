#this script is used to convert output from GBS_SNP_CROP pipeline to a
#more R compatible CSV. There is already a tool in GBS_SNP_CROP doing
#the conversion (GBS-SNP-CROP-8.pl) but the output still requires some
#tweaking (it's space/tab separated, it contains no header, and it's
#one row per marker, while I need one column per marker).
#So, we first invoke the GBS-SNP-CROP-8.pl tool, then an R script
#to do the conversion and bring it to a standard format


#--------------------- PARAMETERS ---------------------
#for development purposes these parameters are hard coded, once the 
#code is stable they will be read somehow (prob from conf file) 
CALLED_INFILE = '/gpfs/work/LI06s_Legpipe/results_GBS_SNP_CROP/SNPsCalled/SNPs_genotyping_matrix.txt'
BARCODE_FILE  = '/gss/gss_work/DRES_Legpipe/2016_01_lupin/BarcodesID.txt'
OUTPUT_PREFIX = '/pico/home/userexternal/nnazzica/SNP_calling_results/2016_01_lupin/GBS_SNP_CROP'
GBS_SNP_CROP8 = '/gpfs/work/LI06s_Legpipe/GBS-SNP-CROP/scripts/GBS-SNP-CROP-8.pl'

#--------------------- ALL IMPORTS ---------------------
import subprocess
import os
import tempfile

#--------------------- GBS_SNP_CROP tool 8 ---------------------
cmd = 'perl ' + GBS_SNP_CROP8 + ' -in ' + CALLED_INFILE \
    + ' -out ' + OUTPUT_PREFIX + ' -b ' + BARCODE_FILE + ' -formats R'
subprocess.call(cmd, shell = True)

#data should be in the following intermediate file
intfile = OUTPUT_PREFIX + '.R_in.txt'

#let's try to open it: if the file does not exist an unhandled exception
#is raised and execution stops
f = open(intfile, 'r')
f.close()

#--------------------- tab to csv ---------------------
#we must now operate a few changes in the file just produced:
#- from space separated to comma separated
#- swap rows and cols
#- first line as header
#The easiest way is to run a short R script.
#To avoid problems with paths, we create a R script on the fly, 
#execute it, and then delete it. 

print "Format conversion"
#bufsize = 0 to ensure instant writing.
#delete = False to avoid deleting as soon as it's closed.
f = tempfile.NamedTemporaryFile(bufsize=0, delete=False)

Rcontent = \
  "infile = '" + intfile + "'\n" + \
  "outfile = '" + OUTPUT_PREFIX + ".csv'\n" + \
  """#reading input file
  df = read.table(infile)
  #creating new names for markers, joining the "mockrefgenome" and 
  #the progressive number
  rownames(df) = paste(sep='_', df[,1], df[,2])
  #removing useless columns
  df = df[,c(-1, -2)]
  #transposing
  df = as.data.frame(t(df))
  #saving
  write.csv(df, outfile)
  """
f.write(Rcontent)
f.close()

#executing the R script
subprocess.call("Rscript " + f.name, shell = True)

#deleting the temporary script
os.remove(f.name)

#a little interface, and we are done
print "Created file: " + OUTPUT_PREFIX + ".csv"
