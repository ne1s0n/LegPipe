#this script transforms the genotype file coming from "vcftools -012" filtering
#to a csv ready to be read and processed by R scripts.
#Because of R data management, the genotype table is transposed (we start from a samples x markers
#and we end with a markers x samples)

# PARAMETERS --------------------------------------------------------------
#for development purposes these parameters are hard coded, once the 
#code is stable they will be read somehow (prob from conf file) 
INFILE = '/gpfs/work/LI06s_Legpipe/python_pipe/data/2016_01_lupin/genotypes/genos.012'
INFILE = '~/research/LegPipe.notes/data/2016_01_lupin/GBS_SNP_CROP_pythonpipe/genos.012'
INFILE = '~/research/LegPipe.notes/data/2016_01_lupin/GBS_SNP_CROP_pythonpipe/run2/genos.012'

# CODE --------------------------------------------------------------------
#reading genotypes file
writeLines(paste('reading genotypes from', INFILE))
df = read.table(INFILE, header=FALSE, sep='\t', row.names = 1, na.strings = '-1')

writeLines(paste('reading accessory data'))

#sample names are recovered from indv file
writeLines(paste(' - sample names from .indv file'))
samples = read.table(paste(sep='.', INFILE, 'indv'), header=FALSE)
rownames(df) = samples[,1]

#marker names are recovered from pos file, reporting chromosome and position
writeLines(paste(' - marker names from .pos file'))
markers = read.table(paste(sep='.', INFILE, 'pos'), header=FALSE)
colnames(df) = paste(sep='_', markers[,1], markers[,2])

#ænd we are ready to save
outfile = paste(sep='.', INFILE, 'csv')
writeLines(paste('writing output to', outfile))
write.csv(t(df), outfile)

